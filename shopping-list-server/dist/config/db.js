"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const shopping_item_1 = __importDefault(require("../models/shopping-item"));
const shoppingItems = require("../data/shopping-items.json");
const connectDB = () => __awaiter(void 0, void 0, void 0, function* () {
    mongoose_1.default.set("strictQuery", true);
    yield mongoose_1.default.connect("mongodb://localhost:27017/shoppingList");
    console.log("MongoDB Connected...");
    // setUpShoppingItems();
});
const setUpShoppingItems = () => {
    shopping_item_1.default.deleteMany({}, (res) => {
        console.log(res);
    }, (e) => {
        console.log(e);
    });
    shopping_item_1.default.insertMany(shoppingItems)
        .then((res) => {
        console.log(res);
    })
        .catch((e) => {
        console.log(e);
    });
};
exports.default = connectDB;
