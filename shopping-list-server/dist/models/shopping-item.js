"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ShoppingItemSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    imageSrc: {
        type: String,
        required: true,
    },
    store: {
        type: String,
        required: true,
    },
    secImageSrc: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
});
const ShoppingItemModel = (0, mongoose_1.model)("shoppingItems", ShoppingItemSchema);
exports.default = ShoppingItemModel;
