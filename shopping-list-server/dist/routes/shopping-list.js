"use strict";
// import { Router, Response } from "express";
// import HttpStatusCodes from "http-status-codes";
// import ShoppingItem, { IShoppingItem } from "../models/shopping-item";
// const router: Router = Router();
// router.post(
//   "/",
//   async (req: Request, res: Response) => {
//     const { firstName, lastName, username } = req.body;
//     // Build profile object based on TProfile
//     const profileFields: TProfile = {
//       user: req.userId,
//       firstName,
//       lastName,
//       username,
//     };
//     try {
//       let user: IUser = await User.findOne({ _id: req.userId });
//       if (!user) {
//         return res.status(HttpStatusCodes.BAD_REQUEST).json({
//           errors: [
//             {
//               msg: "User not registered",
//             },
//           ],
//         });
//       }
//       let profile: IProfile = await Profile.findOne({ user: req.userId });
//       if (profile) {
//         // Update
//         profile = await Profile.findOneAndUpdate(
//           { user: req.userId },
//           { $set: profileFields },
//           { new: true }
//         );
//         return res.json(profile);
//       }
//       // Create
//       profile = new Profile(profileFields);
//       await profile.save();
//       res.json(profile);
//     } catch (err) {
//       console.error(err.message);
//       res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
//     }
//   }
// );
// // @route   GET api/profile
// // @desc    Get all profiles
// // @access  Public
// router.get("/", async (_req: Request, res: Response) => {
//   try {
//     const profiles: IProfile[] = await Profile.find().populate("user", [
//       "avatar",
//       "email",
//     ]);
//     res.json(profiles);
//   } catch (err) {
//     console.error(err.message);
//     res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
//   }
// });
// router.get("/shopping-list", async (req: Request, res: Response) => {
//   try {
//     const ShoppingItems: IShoppingItem = await ShoppingItem.find();
//     if (!ShoppingItems)
//       return res
//         .status(HttpStatusCodes.BAD_REQUEST)
//         .json({ msg: "shopping-items not found" });
//     res.json(ShoppingItem);
//   } catch (err) {
//     console.error(err.message);
//     if (err.kind === "ObjectId") {
//       return res
//         .status(HttpStatusCodes.BAD_REQUEST)
//         .json({ msg: "Profile not found" });
//     }
//     res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
//   }
// });
// router.delete("/", async (req: Request, res: Response) => {
//   try {
//     await ShoppingItem.findOneAndRemove({ _id: req._id });
//     res.json({ msg: "ShoppingItem removed" });
//   } catch (err) {
//     console.error(err.message);
//     res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
//   }
// });
// export default router;
