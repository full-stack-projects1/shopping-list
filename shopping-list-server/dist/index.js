"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = __importDefault(require("./config/db"));
const shopping_item_1 = __importDefault(require("./models/shopping-item"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = require("mongoose");
const app = (0, express_1.default)();
const corsOptions = {
    origin: "*",
    credentials: true,
    optionSuccessStatus: 200,
};
app.use((0, cors_1.default)(corsOptions));
app.use(express_1.default.json());
app.get("/", (_req, res) => {
    res.send("API Running");
});
app.get("/shopping-items", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const shoppingItems = yield shopping_item_1.default.find({});
    res.json(shoppingItems);
}));
app.post("/shopping-items", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const cartList = Object.assign(Object.assign({}, req.body.fullName), { products: req.body.email, total: req.body.cartList });
    yield mongoose_1.connection.collection("cart-list").insertOne(cartList);
    res.sendStatus(200);
}));
const port = 5000;
const startServer = () => __awaiter(void 0, void 0, void 0, function* () {
    yield (0, db_1.default)();
    app.listen(port, () => console.log(`Server started on port ${port}`));
});
startServer();
