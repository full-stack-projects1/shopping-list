import mongoose, { connect } from "mongoose";
import ShoppingItemModel from "../models/shopping-item";
const shoppingItems = require("../data/shopping-items.json");

const connectDB = async () => {
  mongoose.set("strictQuery", true);
   await mongoose.connect("mongodb://127.0.0.1:27017/shopping-list");
  console.log("MongoDB Connected...");

  setUpShoppingItems();
};

const setUpShoppingItems = () => {
  ShoppingItemModel.deleteMany(
    {},
    (res: any) => {
      console.log(res);
    },
    (e) => {
      console.log(e);
    }
  );
  ShoppingItemModel.insertMany(shoppingItems)
    .then((res: any) => {
      console.log(res);
    })
    .catch((e) => {
      console.log(e);
    });
};

export default connectDB;
