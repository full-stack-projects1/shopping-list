import express, { Request, Response } from "express";
import connectDB from "./config/db";
import ShoppingItemModel from "./models/shopping-item";
import cors from "cors";
import { connection } from "mongoose";

const app = express();

const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

app.use(express.json());

app.get("/", (_req, res) => {
  res.send("API Running");
});

app.get("/shopping-items", async (req: Request, res: Response) => {
  const shoppingItems = await ShoppingItemModel.find({});
  res.json(shoppingItems);
});

app.post("/shopping-items", async (req: Request, res: Response) => {
  const cartList = {
    fullName: req.body.fullName,
    mail: req.body.mail,
    cartItems: req.body.cartItems,
  };
  await connection.collection("cart-list").insertOne(cartList);
  res.sendStatus(200);
});

const port = 5000;

const startServer = async () => {
  await connectDB();

  app.listen(port, () => console.log(`Server started on port ${port}`));
};

startServer();
