import { model, Schema } from "mongoose";

export type IShoppingItem = {
  name: string;
  imageSrc: string;
  price: number;
  store: string;
  secImageSrc: string;
  description: string;
};

const ShoppingItemSchema: Schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  imageSrc: {
    type: String,
    required: true,
  },
  store: {
    type: String,
    required: true,
  },
  secImageSrc: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

const ShoppingItemModel = model("shoppingItems", ShoppingItemSchema);

export default ShoppingItemModel;
