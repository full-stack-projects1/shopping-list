import axios from "axios";
import { ICartList } from "../interfaces/cartList.interface";
import { IShoppingItem } from "../interfaces/shoppingItem.interface";

const LOCALHOST_SERVER = "http://localhost:5000";

export const getShoppingList = async () => {
  return (await axios.get<IShoppingItem[]>(`${LOCALHOST_SERVER}/shopping-items`)).data;
};

export const saveShoppingList = async (cartList : ICartList) => {
  return await axios.post<ICartList>(`${LOCALHOST_SERVER}/shopping-items`, {
    ...cartList,
  });
};

