import ShppingList from "../components/ShoppingList";
import React, { useEffect, useState } from "react";
import AppNavBar from "../components/AppNavBar";
import { IShoppingItem } from "../interfaces/shoppingItem.interface";
import DisplayShppingItem from "../components/DisplayShoppingItem";
import CartList from "../components/CartList";
import { makeStyles } from "@material-ui/core";
import { getShoppingList } from "../api/ShoppingList.api";
import { ICartItem } from "../interfaces/cartItem.interface";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    height: "100%",
    minWidth: "100vw",
    width: "100%",
    direction: "rtl",
    backgroundColor: "#0d080b",
    top: "0px",
    right: "0px",
    bottom: "0px",
    left: "0px",
    overflow: "hidden",
  },
}));

const Layout = () => {
  const [shoppingList, setShoppingList] = useState([] as IShoppingItem[]);
  const [selectedShoppingItem, setSelectedShoppingItem] = useState(
    {} as IShoppingItem
  );
  const [cartList, setCartList] = useState([] as ICartItem[]);
  const [isItemSelected, setIsItemSelected] = useState(false);
  const [isCartListSelected, setIsCartListSelected] = useState(false);
  const classes = useStyles();

  const onItemClicked = (itemName: string) => {
    const selectedItem = shoppingList.find((item) => item.name === itemName);

    if (selectedItem) {
      setSelectedShoppingItem(selectedItem);
      setIsItemSelected(true);
    }
  };

  const goToCartItems = () => {
    setIsCartListSelected(true);
    setIsItemSelected(false);
  };

  const backToShoppingList = () => {
    setIsCartListSelected(false);
    setIsItemSelected(false);
  };

  const addToCart = (shoppingItem: IShoppingItem) => {
    cartList.push({ shoppingItem, id: cartList.length });
    setCartList(cartList);
  };

  const removeFromCart = (deletedId: number) => {
    const updatedCart = cartList.filter(({ id }) => deletedId !== id);
    setCartList(updatedCart);
  };

  const emptyCartList = () => {
    setCartList([]);
    setIsCartListSelected(false);
    setIsItemSelected(false);
  };

  useEffect(() => {
    const fetchData = async () => {
      const shoppingList = await getShoppingList();
      setShoppingList(shoppingList);
    };

    fetchData();
  }, []);

  return (
    <div className={classes.root}>
      <AppNavBar
        goToCartItems={goToCartItems}
        backToShoppingList={backToShoppingList}
      />
      {isCartListSelected ? (
        <CartList
          cartList={cartList}
          onItemClicked={onItemClicked}
          removeFromCart={removeFromCart}
          emptyCartList={emptyCartList}
        />
      ) : isItemSelected ? (
        <DisplayShppingItem shoppingItem={selectedShoppingItem} />
      ) : (
        <ShppingList
          shoppingList={shoppingList}
          onItemClicked={onItemClicked}
          addToCart={addToCart}
        />
      )}
    </div>
  );
};

export default Layout;
