export interface IShoppingItem {
    name: string;
    imageSrc: string;
    price: number;
    store: string;
    secImageSrc: string;
    description: string;
}