import { ICartItem } from "./cartItem.interface";

export interface ICartList {
  cartItems: ICartItem[];
  fullName: string;
  mail: string;
}
