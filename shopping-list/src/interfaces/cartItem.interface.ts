import { IShoppingItem } from "./shoppingItem.interface";

export interface ICartItem {
  shoppingItem: IShoppingItem;
  id: number;
}
