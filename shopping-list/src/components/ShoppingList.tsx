import { makeStyles, Grid, Typography } from "@material-ui/core";
import ShppingItem from "./ShoppingItem";
import { IShoppingItem } from "../interfaces/shoppingItem.interface";

interface IShoppingItemProps {
  shoppingList: IShoppingItem[];
  onItemClicked: (itemName: string) => void;
  addToCart: (shoppingItem: IShoppingItem) => void;
}

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "50px",
    direction: "rtl",
  },
  title: {
    color: "white",
    marginTop: "30px"
  },
}));

const ShppingList = (props: IShoppingItemProps) => {
  const { shoppingList, onItemClicked, addToCart } = props;
  const classes = useStyles();

  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      alignItems="center"
      justifyContent="center"
    >
      <Grid item>
        <Typography variant="h5" component="h2" className={classes.title}>
          כל המוצרים
        </Typography>
      </Grid>
      <Grid
        container
        direction="row"
        alignContent="center"
        alignItems="center"
        justifyContent="center"
        spacing={2}
        className={classes.container}
      >
        {shoppingList.map((item, index) => {
          return (
            <ShppingItem
              key={index}
              shoppingItem={item}
              onItemClicked={onItemClicked}
              addToCart={addToCart}
            />
          );
        })}
      </Grid>
    </Grid>
  );
};

export default ShppingList;
