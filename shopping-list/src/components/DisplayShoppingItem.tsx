import { makeStyles, Grid, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { IShoppingItem } from "../interfaces/shoppingItem.interface";

interface IShoppingItemProps {
  shoppingItem: IShoppingItem;
}

const useStyles = makeStyles((theme) => ({
  item: {
    textAlign: "center",
    marginTop: "50px",
    backgroundColor: "white",
    padding: "15px",
    display: "flex",
    minWidth: "600px",
    direction: "rtl",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: "50px"
  },
  image: {
    height: "200px",
    width: "200px",
    borderRadius: "50px",
  },
}));

const DisplayShppingItem = (props: IShoppingItemProps) => {
  const { shoppingItem } = props;
  const classes = useStyles();

  useEffect(() => {}, []);

  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      alignContent="center"
      direction="row"
      className={classes.item}
    >
      <Grid item>
        <Grid container direction="column">
          <Grid item>
            <Typography gutterBottom variant="h5" component="div">
              {shoppingItem.name}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2">{shoppingItem.price} $</Typography>
          </Grid>
          <Typography variant="h5" component="h2">
            החנות שבה נמכר : {shoppingItem.store}
          </Typography>
          <Typography variant="h6" component="h2">
            {shoppingItem.description}
          </Typography>
        </Grid>
      </Grid>
      <Grid
        item
        container
        alignContent="center"
        justifyContent="center"
        alignItems="center"
      >
        <img
          src={shoppingItem.imageSrc}
          alt={shoppingItem.name}
          className={classes.image}
        />
        <img
          src={shoppingItem.secImageSrc}
          alt={shoppingItem.name}
          className={classes.image}
        />
      </Grid>
    </Grid>
  );
};

export default DisplayShppingItem;
