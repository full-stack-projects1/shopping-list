import {
  AppBar,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import Home from "@material-ui/icons/Home";

import React, { useEffect } from "react";

interface IAppNavBarProps {
  goToCartItems: () => void;
  backToShoppingList: () => void;
}

const useStyles = makeStyles((theme) => ({
  navBar: {
    backgroundColor: "white",
    color: "black",
  },
  iconButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const AppNavBar = (props: IAppNavBarProps) => {
  const { goToCartItems, backToShoppingList } = props;
  const classes = useStyles();

  useEffect(() => {}, []);

  return (
    <AppBar position="static" className={classes.navBar}>
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          חנות הקניות שלי
        </Typography>
        <IconButton
          edge="start"
          className={classes.iconButton}
          color="inherit"
          aria-label="cart"
          onClick={() => goToCartItems()}
        >
          <ShoppingCart />
        </IconButton>

        <IconButton
          edge="start"
          className={classes.iconButton}
          color="inherit"
          aria-label="cart"
          onClick={() => backToShoppingList()}
        >
          <Home />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default AppNavBar;
