import {
  makeStyles,
  Grid,
  Typography,
  Button,
  FormControl,
  Input,
  InputAdornment,
  FormHelperText,
} from "@material-ui/core";
import ShppingItem from "./ShoppingItem";
import { ICartItem } from "../interfaces/cartItem.interface";
import { ChangeEvent, useState } from "react";
import { saveShoppingList } from "../api/ShoppingList.api";

interface IShoppingItemProps {
  cartList: ICartItem[];
  onItemClicked: (itemName: string) => void;
  removeFromCart: (id: number) => void;
  emptyCartList: () => void;
}

const useStyles = makeStyles((theme) => ({
  container: {
    width: "600px",
    height: "400px",
    marginTop: "20px",
    overflowY: "scroll",
  },
  title: {
    color: "white",
    marginTop: "20px",
  },
  send: {
    marginTop: "50px",
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(3),
    backgroundColor: "white",
  },
}));

const CartList = (props: IShoppingItemProps) => {
  const { cartList, onItemClicked, removeFromCart, emptyCartList } = props;

  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");

  const classes = useStyles();

  const getTotalPrice = (): number => {
    let price = 0;

    cartList.forEach(({ shoppingItem }) => {
      price += shoppingItem.price;
    });

    return price;
  };

  const saveCartList = () => {
    saveShoppingList({
      fullName,
      mail: email,
      cartItems: cartList,
    });

    setEmail("");
    setFullName("");
    emptyCartList();
  };

  const totalPrice = getTotalPrice();

  const handleChangeFullName = (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setFullName(event.target.value);
  };

  const handleChangeEmail = (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setEmail(event.target.value);
  };

  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      alignItems="center"
      justifyContent="center"
    >
      <Grid item className={classes.title}>
        <Typography variant="h5" component="h2">
          כרגע בסל הקניות :
        </Typography>
        <Typography variant="h5" component="h2">
          סכום הסל : {totalPrice} $
        </Typography>
      </Grid>
      <Grid
        container
        direction="row"
        spacing={2}
        alignItems="center"
        justifyContent="center"
        className={classes.container}
      >
        {cartList.map(({ shoppingItem, id }, index) => {
          return (
            <ShppingItem
              key={index}
              shoppingItem={shoppingItem}
              id={id}
              onItemClicked={onItemClicked}
              removeFromCart={removeFromCart}
            />
          );
        })}
      </Grid>
      <FormControl className={classes.textField}>
        <Input
          value={fullName}
          onChange={handleChangeFullName}
          endAdornment={<InputAdornment position="end"></InputAdornment>}
          aria-describedby="full-name-helper-text"
          inputProps={{
            "aria-label": "שם מלא",
          }}
        />
        <FormHelperText id="full-name-helper-text">שם מלא</FormHelperText>
      </FormControl>
      <FormControl className={classes.textField}>
        <Input
          value={email}
          onChange={handleChangeEmail}
          endAdornment={<InputAdornment position="end"></InputAdornment>}
          aria-describedby="mail-helper-text"
          inputProps={{
            "aria-label": "מייל",
          }}
        />
        <FormHelperText id="mail-helper-text">מייל</FormHelperText>
      </FormControl>

      <Button
        variant="contained"
        className={classes.send}
        onClick={saveCartList}
      >
        שלח
      </Button>
    </Grid>
  );
};

export default CartList;
