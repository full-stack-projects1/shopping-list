import { makeStyles, Grid, Typography, IconButton } from "@material-ui/core";
import React, { useEffect } from "react";
import { IShoppingItem } from "../interfaces/shoppingItem.interface";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import RemoveShoppingCartIcon from "@material-ui/icons/RemoveShoppingCart";

interface IShoppingItemProps {
  shoppingItem: IShoppingItem;
  onItemClicked: (itemName: string) => void;
  addToCart?: (shoppingItem: IShoppingItem) => void;
  removeFromCart?: (id: number) => void;
  id?: number;
}

const useStyles = makeStyles((theme) => ({
  container: {
    textAlign: "center",
    marginBottom: "10px",
    cursor: "pointer",
    direction: "rtl",
    backgroundColor: "white",
    padding: "15px",
    margin: "10px",
  },
  item: {
    display: "flex",
    width: "300px",
  },
  itemData: {
    width: "100px",
    marginLeft: "20px",
    marginTop: "10px",
  },
  image: {
    height: "100px",
    width: "100px",
    marginBottom: "15px",
  },
  iconButton: {
    marginRight: theme.spacing(2),
    marginTop: "30px",
  },
}));

const ShppingItem = (props: IShoppingItemProps) => {
  const { shoppingItem, onItemClicked, addToCart, removeFromCart, id } = props;
  const classes = useStyles();
  const cartId = id ? id : 0;

  useEffect(() => {}, []);

  return (
    <Grid item className={classes.container}>
      <Grid container direction="row" className={classes.item}>
        <Grid item onClick={() => onItemClicked(shoppingItem.name)}>
          <img
            src={shoppingItem.imageSrc}
            alt={shoppingItem.name}
            className={classes.image}
          />
        </Grid>
        <Grid
          item
          onClick={() => onItemClicked(shoppingItem.name)}
          className={classes.itemData}
        >
          <Grid container direction="column">
            <Grid item>
              <Typography gutterBottom variant="h5" component="div">
                {shoppingItem.name}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2">{shoppingItem.price} $</Typography>
            </Grid>
          </Grid>
        </Grid>
        {addToCart ? (
          <Grid item>
            <IconButton
              edge="start"
              className={classes.iconButton}
              color="inherit"
              aria-label="cart"
              onClick={() => addToCart(shoppingItem)}
            >
              <AddShoppingCartIcon />
            </IconButton>
          </Grid>
        ) : (
          <></>
        )}
        {removeFromCart ? (
          <Grid item>
            <IconButton
              edge="start"
              className={classes.iconButton}
              color="inherit"
              aria-label="cart"
              onClick={() => removeFromCart(cartId)}
            >
              <RemoveShoppingCartIcon />
            </IconButton>
          </Grid>
        ) : (
          <></>
        )}
      </Grid>
    </Grid>
  );
};

export default ShppingItem;
